package lif_service

import (
	"bufio"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"free5gc/lib/http2_util"
	"free5gc/lib/openapi/models"
	"free5gc/lib/path_util"
	"free5gc/src/lif/HttpCallback"
	"free5gc/src/lif/Communication"
	"free5gc/src/lif/Location"
	"free5gc/src/lif/MT"
	"free5gc/src/lif/lif_consumer"
	"free5gc/src/lif/lif_context"
	"free5gc/src/lif/lif_handler"
	"free5gc/src/lif/lif_util"
	"free5gc/src/lif/factory"
	"free5gc/src/lif/logger"
	"free5gc/src/app"
	"os/exec"
	"sync"
)

type LIF struct{}

type (
	// Config information.
	Config struct {
		lifcfg string
	}
)

var config Config

var lifCLi = []cli.Flag{
	cli.StringFlag{
		Name:  "free5gccfg",
		Usage: "common config file",
	},
	cli.StringFlag{
		Name:  "lifcfg",
		Usage: "lif config file",
	},
}

var initLog *logrus.Entry

func init() {
	initLog = logger.InitLog
}

func (*LIF) GetCliCmd() (flags []cli.Flag) {
	return lifCLi
}

func (*LIF) Initialize(c *cli.Context) {

	config = Config{
		lifcfg: c.String("lifcfg"),
	}

	if config.lifcfg != "" {
		factory.InitConfigFactory(path_util.Gofree5gcPath(config.lifcfg))
	} else {
		factory.InitConfigFactory(lif_util.DefaultLifConfigPath)
	}

	initLog.Traceln("LIF debug level(string):", app.ContextSelf().Logger.LIF.DebugLevel)
	if app.ContextSelf().Logger.LIF.DebugLevel != "" {
		initLog.Infoln("LIF debug level(string):", app.ContextSelf().Logger.LIF.DebugLevel)
		level, err := logrus.ParseLevel(app.ContextSelf().Logger.LIF.DebugLevel)
		if err == nil {
			logger.SetLogLevel(level)
		}
	}

	logger.SetReportCaller(app.ContextSelf().Logger.LIF.ReportCaller)

}

func (lif *LIF) FilterCli(c *cli.Context) (args []string) {
	for _, flag := range lif.GetCliCmd() {
		name := flag.GetName()
		value := fmt.Sprint(c.Generic(name))
		if value == "" {
			continue
		}

		args = append(args, "--"+name, value)
	}
	return args
}

func (lif *LIF) Start() {
	initLog.Infoln("Server started")

	router := gin.Default()

	Nlif_Callback.AddService(router)
	initLog.Infoln("Service Added")
	for _, serviceName := range factory.LifConfig.Configuration.ServiceNameList {
		initLog.Infoln("service added. name is %s", serviceName)
		switch models.ServiceName(serviceName) {
		case models.ServiceName_NLIF_COMM:
			Communication.AddService(router)
		case models.ServiceName_NLIF_MT:
			Nlif_MT.AddService(router)
		case models.ServiceName_NLIF_LOC:
			Nlif_Location.AddService(router)
		}
	}

	self := lif_context.LIF_Self()
	lif_util.InitLifContext(self)

	initLog.Infoln("LIF IP address: ", self.HttpIPv4Address, self.HttpIpv4Port)
	addr := fmt.Sprintf("%s:%d", self.HttpIPv4Address, self.HttpIpv4Port)
	
	go lif_handler.Handle()
	// Register to NRF
	profile, err := lif_consumer.BuildNFInstance(self)
	if err != nil {
		initLog.Error("Build LIF Profile Error")
	}

	_, self.NfId, _ = lif_consumer.SendRegisterNFInstance(self.NrfUri, self.NfId, profile)
	
	server, err := http2_util.NewServer(addr, lif_util.LifLogPath, router)
	initLog.Infoln("create server")
	if err == nil && server != nil {
		initLog.Infoln(server.ListenAndServeTLS(lif_util.LifPemPath, lif_util.LifKeyPath))
	}
}

func (lif *LIF) Exec(c *cli.Context) error {

	//LIF.Initialize(cfgPath, c)

	initLog.Traceln("args:", c.String("lifcfg"))
	args := lif.FilterCli(c)
	initLog.Traceln("filter: ", args)
	command := exec.Command("./lif", args...)

	stdout, err := command.StdoutPipe()
	if err != nil {
		initLog.Fatalln(err)
	}
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		in := bufio.NewScanner(stdout)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	stderr, err := command.StderrPipe()
	if err != nil {
		initLog.Fatalln(err)
	}
	go func() {
		in := bufio.NewScanner(stderr)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	go func() {
		if err := command.Start(); err != nil {
			initLog.Errorf("LIF Start error: %v", err)
		}
		wg.Done()
	}()

	wg.Wait()

	return err
}
