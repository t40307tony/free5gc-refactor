package Nlif_Location_test

import (
	"context"
	"free5gc/lib/CommonConsumerTestData/LIF/TestLif"
	Nlif_Loc_Clinet "free5gc/lib/Nlif_Location"
	"free5gc/lib/http2_util"
	"free5gc/lib/openapi/common"
	"free5gc/lib/openapi/models"
	Nlif_Loc_Server "free5gc/src/lif/Location"
	"free5gc/src/lif/lif_handler"
	"log"

	"github.com/stretchr/testify/assert"

	"testing"
	"time"
)

func sendRequestAndPrintResult(client *Nlif_Loc_Clinet.APIClient, supi string, request models.RequestLocInfo) {
	ueContextInfo, httpResponse, err := client.IndividualUEContextDocumentApi.ProvideLocationInfo(context.Background(), supi, request)
	if err != nil {
		if httpResponse == nil {
			log.Panic(err)
		} else if err.Error() != httpResponse.Status {
			log.Panic(err)
		} else {
			var probelmDetail models.ProblemDetails
			probelmDetail = err.(common.GenericOpenAPIError).Model().(models.ProblemDetails)
			TestLif.Config.Dump(probelmDetail)
		}
	} else {
		TestLif.Config.Dump(ueContextInfo)
	}
}

func TestProvideLocationInfo(t *testing.T) {
	go func() {
		router := Nlif_Loc_Server.NewRouter()
		server, err := http2_util.NewServer(":29518", TestLif.LifLogPath, router)
		if err == nil && server != nil {
			err = server.ListenAndServeTLS(TestLif.LifPemPath, TestLif.LifKeyPath)
		}
		assert.True(t, err == nil, err.Error())
	}()

	go lif_handler.Handle()
	TestLif.LifInit()
	TestLif.UeAttach(models.AccessType__3_GPP_ACCESS)
	time.Sleep(100 * time.Millisecond)
	configuration := Nlif_Loc_Clinet.NewConfiguration()
	configuration.SetBasePath("https://127.0.0.1:29518")
	client := Nlif_Loc_Clinet.NewAPIClient(configuration)
	ue := TestLif.TestLif.UePool["imsi-2089300007487"]
	ue.Supi = "imsi-2089300007487"
	var requestLocInfo models.RequestLocInfo

	sendRequestAndPrintResult(client, ue.Supi, requestLocInfo)

	requestLocInfo.Req5gsLoc = true
	requestLocInfo.ReqCurrentLoc = true
	requestLocInfo.ReqRatType = true
	requestLocInfo.ReqTimeZone = true
	sendRequestAndPrintResult(client, ue.Supi, requestLocInfo)

	// 404 CONTEXT_NOT_FOUND
	sendRequestAndPrintResult(client, "imsi-0010202", requestLocInfo)
}
