/*
 * LIF Configuration Factory
 */

package factory

import (
	//"free5gc/lib/openapi/models"
	"free5gc/src/lif/lif_context"
)

type Configuration struct {
	LifName string `yaml:"lifName,omitempty"`

	Sbi *Sbi `yaml:"sbi,omitempty"`

	ServiceNameList []string `yaml:"serviceNameList,omitempty"`

	SupportDnnList []string `yaml:"supportDnnList,omitempty"`

	NrfUri string `yaml:"nrfUri,omitempty"`

	Security *Security `yaml:"security,omitempty"`

	NetworkName lif_context.NetworkName `yaml:"networkName,omitempty"`

	T3502 int `yaml:"t3502,omitempty"`

	T3512 int `yaml:"t3512,omitempty"`

	Non3gppDeregistrationTimer int `yaml:"mon3gppDeregistrationTimer,omitempty"`
}
