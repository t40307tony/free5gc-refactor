package lif_handler_test

import (
	"free5gc/lib/CommonConsumerTestData/LIF/TestLif"
	"free5gc/lib/ngap"
	"free5gc/lib/openapi/models"
	"free5gc/src/lif/lif_handler"
	"free5gc/src/lif/lif_handler/lif_message"
	"free5gc/src/lif/lif_ngap"
	"free5gc/src/test/ngapTestpacket"
	"testing"
	"time"
)

func TestHandler(t *testing.T) {
	go lif_handler.Handle()
	TestLif.SctpSever()
	TestLif.LifInit()
	TestLif.SctpConnectToServer(models.AccessType__3_GPP_ACCESS)
	message := ngapTestpacket.BuildNGSetupRequest()
	ngapMsg, err := ngap.Encoder(message)
	if err != nil {
		lif_ngap.Ngaplog.Errorln(err)
	}
	msg := lif_message.HandlerMessage{}
	msg.Event = lif_message.EventNGAPMessage
	msg.NgapAddr = TestLif.Laddr.String()
	msg.Value = ngapMsg
	lif_message.SendMessage(msg)

	time.Sleep(100 * time.Millisecond)

}
