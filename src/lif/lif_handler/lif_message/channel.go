package lif_message

import (
	"sync"
)

var LifChannel chan HandlerMessage
var mtx sync.Mutex

const (
	MaxChannel int = 100000
)

func init() {
	// init Pool
	LifChannel = make(chan HandlerMessage, MaxChannel)
}

func SendMessage(msg HandlerMessage) {
	mtx.Lock()
	LifChannel <- msg
	mtx.Unlock()
}
