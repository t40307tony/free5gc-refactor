package lif_message

type Event int

const (
	EventCreateUEContext Event = iota
	EventProvideDomainSelectionInfo
	EventProvideLocationInfo
	EventGMMT3513
	EventPagingUENotify
)
