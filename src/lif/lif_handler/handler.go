package lif_handler

import (
	"free5gc/lib/openapi/models"
	"free5gc/src/lif/lif_handler/lif_message"
	"free5gc/src/lif/lif_producer"
	"free5gc/src/lif/logger"
	"time"

	"github.com/sirupsen/logrus"
)

var HandlerLog *logrus.Entry

func init() {
	// init Pool
	HandlerLog = logger.HandlerLog
}

func Handle() {
	for {
		select {
		case msg, ok := <-lif_message.LifChannel:
			if ok {
				switch msg.Event {
				/*case lif_message.EventGMMT3513:
					lifUe, ok := msg.Value.(*lif_context.LifUe)
					if !ok || lifUe == nil {
						HandlerLog.Warn("Timer T3513 Parameter Error\n")
					}
					lifUe.PagingRetryTimes++
					logger.GmmLog.Infof("Paging UE[%s] expired for the %dth times", lifUe.Supi, lifUe.PagingRetryTimes)
					if lifUe.PagingRetryTimes >= lif_context.MaxPagingRetryTime {
						logger.GmmLog.Warnf("Paging to UE[%s] failed. Stop paging", lifUe.Supi)
						//if lifUe.OnGoing[models.AccessType__3_GPP_ACCESS].Procedure != lif_context.OnGoingProcedureN2Handover {
						//	lif_producer_callback.SendN1N2TransferFailureNotification(lifUe, models.N1N2MessageTransferCause_UE_NOT_RESPONDING)
						//} //AMF need to do that not LIF
						lif_util.ClearT3513(lifUe)
					} else {
						ngap_message.SendPaging(lifUe, lifUe.LastPagingPkg)
						_, _ := lif_consumer.SendPagingRequest(ueContextId, nil, lifUe.LastPagingPkg)
					}	*/			
				/*case lif_message.EventProvideDomainSelectionInfo:
					infoClass := msg.HTTPRequest.Query.Get("info-class")
					ueContextId := msg.HTTPRequest.Params["ueContextId"]
					HandlerLog.Traceln("handle Provide Domain Selection Start")
					lif_producer.HandleProvideDomainSelectionInfoRequest(msg.ResponseChan, ueContextId, infoClass)
					HandlerLog.Traceln("handle Provide Domain Selection End")
				case lif_message.EventProvideLocationInfo:
					ueContextId := msg.HTTPRequest.Params["ueContextId"]
					lif_producer.HandleProvideLocationInfoRequest(msg.ResponseChan, ueContextId, msg.HTTPRequest.Body.(models.RequestLocInfo))*/
				case lif_message.EventPagingUENotify:
					ueContextId := msg.HTTPRequest.Params["ueContextId"]
					reqUri := msg.HTTPRequest.Params["reqUri"]
					HandlerLog.Infof("LIF receive the AMF forward message [paging notify].")
					lif_producer.HandlePagingNotify(msg.ResponseChan, ueContextId, reqUri, msg.HTTPRequest.Body.(models.N1N2MessageTransferRequest))
				default:
					HandlerLog.Warnf("Event[%d] has not implemented", msg.Event)
				}
			} else {
				HandlerLog.Errorln("Channel closed!")
			}

		case <-time.After(time.Second * 1):

		}
	}
}
