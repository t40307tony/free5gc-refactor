package lif_context

import (
	"fmt"
	"free5gc/lib/openapi/models"
	"free5gc/src/lif/logger"
	"strconv"
	"strings"
	"time"
)

var lifContext = LIFContext{}
var TmsiGenerator int32 = 0
var lifUeNgapIdGenerator int64 = 0

func init() {
	LIF_Self().Name = "lif"
	LIF_Self().UriScheme = models.UriScheme_HTTPS
	LIF_Self().RelativeCapacity = 0xff
	LIF_Self().LIFStatusSubscriptionIDGenerator = 1
	LIF_Self().LIFStatusSubscriptions = make(map[string]*models.SubscriptionData)
	LIF_Self().NfService = make(map[models.ServiceName]models.NfService)
	LIF_Self().NetworkName.Full = "free5GC"
}

type LIFContext struct {
	RelativeCapacity                 int64
	NfId                             string
	Name                             string
	NfService                        map[models.ServiceName]models.NfService // use ServiceName as key, nfservice that lif support
	UriScheme                        models.UriScheme
	HttpIpv4Port                     int
	HttpIPv4Address                  string
	HttpIPv6Address                  string
	TNLWeightFactor                  int64
	SupportDnnLists                  []string
	LIFStatusSubscriptionIDGenerator int
	LIFStatusSubscriptions           map[string]*models.SubscriptionData
	NrfUri                           string
	SecurityAlgorithm                SecurityAlgorithm
	NetworkName                      NetworkName
	//NgapIpList                       []string // NGAP Server IP
	T3502Value                       int      // unit is second
	T3512Value                       int      // unit is second
	//Non3gppDeregistrationTimerValue  int      // unit is second
}

type LIFContextEventSubscription struct {
	IsAnyUe           bool
	IsGroupUe         bool
	UeSupiList        []string
	Expiry            *time.Time
	//EventSubscription models.LifEventSubscription
}

type NetworkName struct {
	Full  string `yaml:"full"`
	Short string `yaml:"short,omitempty"`
}

type SecurityAlgorithm struct {
	IntegrityOrder []uint8 // 8bits(NIA1, NIA2, NIA3 , EIA0, EIA1, EIA2, EIA3, ..)
	CipheringOrder []uint8 // 8bits(NEA1, NEA2, NEA3 , EEA0, EEA1, EEA2, EEA3, ..)
}

/*func NewPlmnSupportItem() (item PlmnSupportItem) {
	item.SNssaiList = make([]models.Snssai, 0, MaxNumOfSlice)
	return
}

func (context *LIFContext) TmsiAlloc() int32 {
	TmsiGenerator %= math.MaxInt32
	TmsiGenerator++
	for {
		if _, double := context.TmsiPool[TmsiGenerator]; double {
			TmsiGenerator++
		} else {
			break
		}
	}
	return TmsiGenerator
}*/

/*func (context *LIFContext) LifUeNgapIdAlloc() int64 {
	lifUeNgapIdGenerator %= MaxValueOfLifUeNgapId
	lifUeNgapIdGenerator++
	for {
		if _, double := context.RanUePool[lifUeNgapIdGenerator]; double {
			lifUeNgapIdGenerator++
		} else {
			break
		}
	}
	return lifUeNgapIdGenerator
}*/

/*func (context *LIFContext) AllocateGutiToUe(ue *LifUe) {

	// if ue has a previous tmsi/guti, remove it first
	if ue.Tmsi != 0 {
		delete(context.TmsiPool, ue.Tmsi)
		delete(context.GutiPool, ue.Guti)
	}

	servedGuami := context.ServedGuamiList[0]
	ue.Tmsi = context.TmsiAlloc()

	plmnID := servedGuami.PlmnId.Mcc + servedGuami.PlmnId.Mnc
	tmsiStr := fmt.Sprintf("%08x", ue.Tmsi)
	ue.Guti = plmnID + servedGuami.LifId + tmsiStr

	context.TmsiPool[ue.Tmsi] = ue
	context.GutiPool[ue.Guti] = ue
}*/

/*func (context *LIFContext) AllocateRegistrationArea(ue *LifUe, anType models.AccessType) {

	// clear the previous registration area if need
	if len(ue.RegistrationArea[anType]) > 0 {
		ue.RegistrationArea[anType] = nil
	}

	// allocate a new tai list as a registration area to ue
	// TODO: algorithm to choose TAI list
	for _, supportTai := range context.SupportTaiLists {
		if reflect.DeepEqual(supportTai, ue.Tai) {
			ue.RegistrationArea[anType] = append(ue.RegistrationArea[anType], supportTai)
			break
		}
	}
}

func (context *LIFContext) AddLifUeToUePool(ue *LifUe, supi string) {
	if len(supi) == 0 {
		logger.ContextLog.Errorf("Supi is nil")
	}
	ue.Supi = supi
	context.UePool[ue.Supi] = ue
}

func (context *LIFContext) NewLifUe(supi string) *LifUe {
	ue := LifUe{}
	ue.init()

	if supi != "" {
		context.AddLifUeToUePool(&ue, supi)
	}

	context.AllocateGutiToUe(&ue)

	return &ue
}

func (context *LIFContext) NewLifRan(conn net.Conn) *LifRan {
	ran := LifRan{}
	ran.SupportedTAList = make([]SupportedTAI, 0, MaxNumOfTAI*MaxNumOfBroadcastPLMNs)
	context.LifRanPool[conn.RemoteAddr().String()] = &ran
	ran.Conn = conn
	return &ran
}

func (context *LIFContext) InSupportDnnList(targetDnn string) bool {
	for _, dnn := range context.SupportDnnLists {
		if dnn == targetDnn {
			return true
		}
	}
	return false
}

func (context *LIFContext) LifUeFindByGuti(targetGuti string) *LifUe {
	if ue, ok := context.GutiPool[targetGuti]; ok {
		return ue
	}
	return nil
}

func (context *LIFContext) LifRanFindByRanId(ranNodeId models.GlobalRanNodeId) *LifRan {

	for _, lifRan := range context.LifRanPool { // lifRan = context.LifRanPool[i]
		switch lifRan.RanPresent {
		case RanPresentGNbId:
			if lifRan.RanId.GNbId.GNBValue == ranNodeId.GNbId.GNBValue {
				return lifRan
			}
		case RanPresentNgeNbId:
			if lifRan.RanId.NgeNbId == ranNodeId.NgeNbId {
				return lifRan
			}
		case RanPresentN3IwfId:
			if lifRan.RanId.N3IwfId == ranNodeId.N3IwfId {
				return lifRan
			}
		}
	}

	return nil
}

func (context *LIFContext) RanUeFindByLifUeNgapID(lifUeNgapID int64) *RanUe {
	if ue, ok := context.RanUePool[lifUeNgapID]; ok {
		return ue
	}
	return nil
}*/

func (context *LIFContext) GetIPv4Uri() string {
	return fmt.Sprintf("%s://%s:%d", context.UriScheme, context.HttpIPv4Address, context.HttpIpv4Port)
}

func (context *LIFContext) InitNFService(serivceName []string, version string) {
	tmpVersion := strings.Split(version, ".")
	versionUri := "v" + tmpVersion[0]
	logger.ContextLog.Info("InitNFService")
	for index, nameString := range serivceName {
		name := models.ServiceName(nameString)
		context.NfService[name] = models.NfService{
			ServiceInstanceId: strconv.Itoa(index),
			ServiceName:       name,
			Versions: &[]models.NfServiceVersion{
				{
					ApiFullVersion:  version,
					ApiVersionInUri: versionUri,
				},
			},
			Scheme:          context.UriScheme,
			NfServiceStatus: models.NfServiceStatus_REGISTERED,
			ApiPrefix:       context.GetIPv4Uri(),
			IpEndPoints: &[]models.IpEndPoint{
				{
					Ipv4Address: context.HttpIPv4Address,
					Transport:   models.TransportProtocol_TCP,
					Port:        int32(context.HttpIpv4Port),
				},
			},
		}
	}
	logger.ContextLog.Info("InitNFService end.")
}

// Reset LIF Context
func (context *LIFContext) Reset() {
	for key := range context.NfService {
		delete(context.NfService, key)
	}
	context.RelativeCapacity = 0xff
	context.NfId = ""
	context.UriScheme = models.UriScheme_HTTPS
	context.HttpIpv4Port = 0
	context.HttpIPv4Address = ""
	context.HttpIPv6Address = ""
	context.Name = "lif"
	context.NrfUri = ""
}

// Create new LIF context
func LIF_Self() *LIFContext {
	return &lifContext
}
