package lif_consumer_test

import (
	"flag"
	"github.com/antihax/optional"
	"github.com/urfave/cli"
	"go.mongodb.org/mongo-driver/bson"
	"free5gc/lib/CommonConsumerTestData/LIF/TestLif"
	"free5gc/lib/MongoDBLibrary"
	"free5gc/lib/Nnrf_NFDiscovery"
	"free5gc/lib/openapi/models"
	"free5gc/src/lif/lif_consumer"
	"free5gc/src/nrf/nrf_service"
	"reflect"
	"testing"
	"time"
)

func nrfInit() {
	flags := flag.FlagSet{}
	c := cli.NewContext(nil, &flags, nil)
	nrf := &nrf_service.NRF{}
	nrf.Initialize(c)
	go nrf.Start()
	time.Sleep(100 * time.Millisecond)
}

func TestSendSearchNFInstances(t *testing.T) {

	nrfInit()

	time.Sleep(200 * time.Millisecond)
	MongoDBLibrary.RestfulAPIDeleteMany("NfProfile", bson.M{})

	// Init LIF
	TestLif.LifInit()

	time.Sleep(100 * time.Millisecond)

	nfprofile, err := lif_consumer.BuildNFInstance(TestLif.TestLif)
	if err != nil {
		t.Error(err.Error())
	}

	uri, err1 := lif_consumer.SendRegisterNFInstance(TestLif.TestLif.NrfUri, TestLif.TestLif.NfId, nfprofile)
	if err1 != nil {
		t.Error(err1.Error())
	} else {
		TestLif.Config.Dump(uri)
	}

	param := Nnrf_NFDiscovery.SearchNFInstancesParamOpts{
		ServiceNames: optional.NewInterface([]models.ServiceName{models.ServiceName_NLIF_COMM}),
	}
	result, err2 := lif_consumer.SendSearchNFInstances(TestLif.TestLif.NrfUri, models.NfType_LIF, models.NfType_LIF, param)
	if err2 != nil {
		t.Error(err1.Error())
	} else if !reflect.DeepEqual(nfprofile, result.NfInstances[0]) {
		t.Error("failed for expected value mismatch")
	}
}
