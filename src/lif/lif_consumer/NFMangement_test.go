package lif_consumer_test

import (
	"go.mongodb.org/mongo-driver/bson"
	"free5gc/lib/CommonConsumerTestData/LIF/TestLif"
	"free5gc/lib/MongoDBLibrary"
	"free5gc/src/lif/lif_consumer"
	"testing"
	"time"
)

func TestRegisterNFInstance(t *testing.T) {

	nrfInit()

	time.Sleep(200 * time.Millisecond)
	MongoDBLibrary.RestfulAPIDeleteMany("NfProfile", bson.M{})

	// Init LIF
	TestLif.LifInit()

	time.Sleep(100 * time.Millisecond)

	nfprofile, err := lif_consumer.BuildNFInstance(TestLif.TestLif)
	if err != nil {
		t.Error(err.Error())
	}

	uri, err1 := lif_consumer.SendRegisterNFInstance(TestLif.TestLif.NrfUri, TestLif.TestLif.NfId, nfprofile)
	if err1 != nil {
		t.Error(err1.Error())
	} else {
		TestLif.Config.Dump(uri)
	}
}
