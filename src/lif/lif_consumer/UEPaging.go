package lif_consumer

import (
	"context"
	"free5gc/lib/openapi/common"
	"free5gc/lib/openapi/models"
	Namf_Location "free5gc/lib/Namf_Location"
	"free5gc/src/lif/lif_context"
	"free5gc/src/lif/logger"
	"free5gc/lib/Nnrf_NFDiscovery"
	"free5gc/src/lif/lif_util"
	//"fmt"
	//"free5gc/src/lif/logger"
)

func SendPagingRequest(ueContextId, reqUri string, body models.N1N2MessageTransferRequest) (problemDetails *models.ProblemDetails, err error) {

	lifSelf := lif_context.LIF_Self()
	// TODO: consider ausf group id, Routing ID part of SUCI
	param := Nnrf_NFDiscovery.SearchNFInstancesParamOpts{
		//Supi: optional.NewString(ue.Supi),
	}
	logger.ConsumerLog.Info("step 1")
	

	resp, err := SendSearchNFInstances(lifSelf.NrfUri, models.NfType_AMF, models.NfType_LIF, param)
	if err != nil {
		logger.ConsumerLog.Error("LIF can not select an AMF by NRF")
		return
	}

	logger.ConsumerLog.Info("step 2")	

	// select the first AUSF, TODO: select base on other info
	var amfUri string
	for _, nfProfile := range resp.NfInstances {
		amfUri = lif_util.SearchNFServiceUri(nfProfile, models.ServiceName_NAMF_LOC, models.NfServiceStatus_REGISTERED)
		if amfUri != "" {
			break
		}
	}
	
	if amfUri == "" {
		//err := fmt.Errorf("LIF can not select an AMF by NRF")
		logger.ConsumerLog.Errorf(err.Error())
		return
	}

	logger.ConsumerLog.Info("amfUri: [%s]", amfUri)	
	configuration := Namf_Location.NewConfiguration()	
	//lifUri := "https://localhost:29519"
	configuration.SetBasePath(amfUri)

	client := Namf_Location.NewAPIClient(configuration)	

	logger.ConsumerLog.Info("ueContextId: ", ueContextId)	

	httpResp, localErr := client.IndividualUEPagingApi.UEPaging(context.Background(), ueContextId, body)

	logger.ConsumerLog.Info("httpResp: ", httpResp)	

	if localErr == nil {
	} else if httpResp != nil {
		if httpResp.Status != localErr.Error() {
			err = localErr
			return
		}
		problem := localErr.(common.GenericOpenAPIError).Model().(models.ProblemDetails)
		problemDetails = &problem
	} else {
		err = common.ReportError("server no response")
	}
	return
}
