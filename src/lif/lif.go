package main

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"free5gc/src/lif/lif_service"
	"free5gc/src/lif/logger"
	"free5gc/src/app"
	"os"
)

var LIF = &lif_service.LIF{}

var appLog *logrus.Entry

func init() {
	appLog = logger.AppLog
}

func main() {
	app := cli.NewApp()
	app.Name = "lif"
	appLog.Infoln(app.Name)
	app.Usage = "-free5gccfg common configuration file -lifcfg lif configuration file"
	app.Action = action
	app.Flags = LIF.GetCliCmd()
	if err := app.Run(os.Args); err != nil {
		logger.AppLog.Errorf("LIF Run error: %v", err)
	}
}

func action(c *cli.Context) {
	app.AppInitializeWillInitialize(c.String("free5gccfg"))
	LIF.Initialize(c)
	LIF.Start()
}
