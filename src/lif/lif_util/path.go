package lif_util

import (
	"free5gc/lib/path_util"
)

var LifLogPath = path_util.Gofree5gcPath("free5gc/lifsslkey.log")
var LifPemPath = path_util.Gofree5gcPath("free5gc/support/TLS/lif.pem")
var LifKeyPath = path_util.Gofree5gcPath("free5gc/support/TLS/lif.key")
var DefaultLifConfigPath = path_util.Gofree5gcPath("free5gc/config/lifcfg.conf")
