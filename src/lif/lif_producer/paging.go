package lif_producer

import (
	"free5gc/lib/openapi/models"
	//"free5gc/src/lif/lif_context"
	"free5gc/src/lif/lif_handler/lif_message"
	"free5gc/src/lif/lif_consumer"
	"free5gc/src/lif/logger"
	"net/http"
	//"strings"
)

func HandlePagingNotify(httpChannel chan lif_message.HandlerResponseMessage, ueContextId, reqUri string, body models.N1N2MessageTransferRequest) {
	//var response models.ProvideLocInfo
	//var problem models.ProblemDetails

	lif_message.SendHttpResponseMessage(httpChannel, nil, http.StatusOK, nil)

	logger.ProducerLog.Info("LIF send Paging Reqeust to AMF")
	
	_, _ = lif_consumer.SendPagingRequest(ueContextId, reqUri, body)

	logger.ProducerLog.Info("LIF receive Paging response from AMF")

	/*if err != nil {
		lif_message.SendHttpResponseMessage(httpChannel, nil, http.StatusInternalServerError, err)
	} else if problemDetails != nil {
		lif_message.SendHttpResponseMessage(httpChannel, nil, http.StatusInternalServerError, problemDetails)
	}*/

	//lif_message.SendHttpResponseMessage(httpChannel, nil, http.StatusOK, nil)
}
