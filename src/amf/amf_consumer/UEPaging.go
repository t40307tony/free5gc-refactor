package amf_consumer

import (
	"context"
	Nlif_Location "free5gc/lib/Nlif_Location"
	"free5gc/lib/openapi/common"
	"free5gc/lib/openapi/models"
	"free5gc/src/amf/amf_context"
	"fmt"
	"free5gc/src/amf/logger"
	"free5gc/lib/Nnrf_NFDiscovery"
	"free5gc/src/amf/amf_util"
	//"github.com/antihax/optional"
)

func SendPagingNotify(ue *amf_context.AmfUe, body models.N1N2MessageTransferRequest) (problemDetails *models.ProblemDetails, err1 error) {
	
	
	//edit by Tony select the ausf by amf.
	//search NF example
	amfSelf := amf_context.AMF_Self()
	// TODO: consider ausf group id, Routing ID part of SUCI
	param := Nnrf_NFDiscovery.SearchNFInstancesParamOpts{
		//Supi: optional.NewString(ue.Supi),
	}
	logger.GmmLog.Info("step 1")
	

	resp, err := SendSearchNFInstances(amfSelf.NrfUri, models.NfType_LIF, models.NfType_AMF, param)
	if err != nil {
		logger.GmmLog.Error("AMF can not select an LIF by NRF")
		return
	}

	logger.GmmLog.Info("step 2")	

	// select the first AUSF, TODO: select base on other info
	var lifUri string
	for _, nfProfile := range resp.NfInstances {
		lifUri = amf_util.SearchNFServiceUri(nfProfile, models.ServiceName_NLIF_LOC, models.NfServiceStatus_REGISTERED)
		if lifUri != "" {
			break
		}
	}
	
	if lifUri == "" {
		err := fmt.Errorf("AMF can not select an LIF by NRF")
		logger.GmmLog.Errorf(err.Error())
		return
	}

	logger.GmmLog.Info("lifUri: [%s]", lifUri)	
	configuration := Nlif_Location.NewConfiguration()	
	//lifUri := "https://localhost:29519"
	configuration.SetBasePath(lifUri)

	client := Nlif_Location.NewAPIClient(configuration)

	//amfSelf := amf_context.AMF_Self()
	//servedGuami := amfSelf.ServedGuamiList[0]

	var ueContextId string
	ueContextId = ue.Supi

	httpResponse, err := client.IndividualUEPagingApi.UEPagingNotify(context.Background(), ueContextId, body)
	if err == nil {
		return
	} else if httpResponse != nil {
		if httpResponse.Status != err.Error() {
			err1 = err
			return
		}
		problem := err.(common.GenericOpenAPIError).Model().(models.ProblemDetails)
		problemDetails = &problem
	} else {
		err1 = common.ReportError("server no response")
	}
	return
}

