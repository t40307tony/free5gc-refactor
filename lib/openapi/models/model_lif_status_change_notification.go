/*
 * Nlif_Communication
 *
 * LIF Communication Service
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package models

type LifStatusChangeNotification struct {
	LifStatusInfoList []LifStatusInfo `json:"lifStatusInfoList,omitempty"`
}
