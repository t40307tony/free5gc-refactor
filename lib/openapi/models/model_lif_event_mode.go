/*
 * Nlif_Communication
 *
 * LIF Communication Service
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package models

import (
	"time"
)

type LifEventMode struct {
	Trigger    LifEventTrigger `json:"trigger"`
	MaxReports int32           `json:"maxReports,omitempty"`
	Expiry     *time.Time      `json:"expiry,omitempty"`
}
