/*
 * Nlif_Communication
 *
 * LIF Communication Service
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package models

type LifEvent struct {
	Type                     LifEventType           `json:"type"`
	ImmediateFlag            bool                   `json:"immediateFlag,omitempty"`
	AreaList                 []LifEventArea         `json:"areaList,omitempty"`
	LocationFilterList       []LocationFilter       `json:"locationFilterList,omitempty"`
	SubscribedDataFilterList []SubscribedDataFilter `json:"subscribedDataFilterList,omitempty"`
}
